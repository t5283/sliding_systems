FROM python:3.8.10
WORKDIR /code
COPY requirements.txt /code/
RUN pip install --upgrade pip && pip install --no-cache -r requirements.txt
COPY source /code
EXPOSE 8010
CMD ["python", "manage.py", "runserver", "0.0.0.0:8010"]