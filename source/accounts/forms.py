from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm

from accounts.models import Profile


class RegistrationForm(UserCreationForm):
    phone_number = forms.CharField(max_length=150, label="Контактный телефон")
    class Meta(UserCreationForm.Meta):
        fields = ["username", "phone_number", "password1", "password2", "groups"]
        labels = {'username': 'Логин', 'phone_number': 'Контактный телефон', 'password1': 'Пароль', 'password2': 'Повторите пароль'}

class UserChangeForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ["username", "first_name", "last_name"]
        labels = {'username': 'Логин', 'first_name': 'Имя', 'last_name': 'Фамилия'}


class ProfileChangeForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ['user']
