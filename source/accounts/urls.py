from django.urls import path, include
from accounts import views as account_views


app_name = "accounts"

urlpatterns = [
    path("accounts/", include("django.contrib.auth.urls")),
    path("create/", account_views.RegisterView.as_view(), name="register"),
    path("<int:pk>/", account_views.UserDetailView.as_view(), name="profile"),
    path('<int:pk>/change/', account_views.UserChangeView.as_view(), name='change'),
    path("<int:pk>/change-password/", account_views.UserPasswordChangeView.as_view(), name="change_password"),
    path("<int:pk>/coefficient/", account_views.CoefficientAddView.as_view(), name="coefficient")
]