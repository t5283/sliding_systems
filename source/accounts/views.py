from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.views import PasswordChangeView
from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse
from django.views import View
from django.views.generic import DetailView, CreateView, UpdateView

from accounts.admin import User
from accounts.forms import RegistrationForm, ProfileChangeForm, UserChangeForm
from accounts.models import Profile
from webapp.models import Coefficient


class UserDetailView(UserPassesTestMixin, DetailView):
    model = get_user_model()
    template_name = 'user_detail.html'
    context_object_name = 'user_obj'

    def test_func(self):
        return self.request.user == self.get_object() or self.request.user.is_superuser

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        users = User.objects.all()
        coefficient = get_object_or_404(Coefficient, pk=1)
        context["users"] = users
        context["coefficient"] = coefficient
        return context

class RegisterView(UserPassesTestMixin, CreateView):
    model = get_user_model()
    template_name = "registration/register.html"
    form_class = RegistrationForm

    def form_valid(self, form):
        user = form.save()
        form.save_m2m()
        Profile.objects.create(user=user)
        return redirect("accounts:profile", pk=self.request.user.pk)

    def test_func(self):
        return self.request.user.is_superuser


class UserChangeView(UserPassesTestMixin, UpdateView):
    model = get_user_model()
    form_class = UserChangeForm
    template_name = 'user_change.html'
    context_object_name = 'user_obj'

    def test_func(self):
        return self.request.user == self.get_object() or self.request.user.is_superuser

    def get_context_data(self, **kwargs):
        if 'profile_form' not in kwargs:
            kwargs['profile_form'] = self.get_profile_form()
        return super().get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        profile_form = self.get_profile_form()
        if form.is_valid() and profile_form.is_valid():
            return self.form_valid(form, profile_form)
        else:
            return self.form_invalid(form, profile_form)

    def form_valid(self, form, profile_form):
        response = super().form_valid(form)
        profile_form.save()
        return response

    def form_invalid(self, form, profile_form):
        context = self.get_context_data(form=form, profile_form=profile_form)
        return self.render_to_response(context)

    def get_profile_form(self):
        form_kwargs = {'instance': self.object.profile}
        if self.request.method == 'POST':
            form_kwargs['data'] = self.request.POST
            form_kwargs['files'] = self.request.FILES
        return ProfileChangeForm(**form_kwargs)

    def get_success_url(self):
        return reverse('accounts:profile', kwargs={'pk': self.object.pk})


class UserPasswordChangeView(UserPassesTestMixin, PasswordChangeView):
    template_name = 'user_password_change.html'

    def test_func(self):
        return self.request.user.pk == self.kwargs["pk"] or self.request.user.is_superuser

    def get_success_url(self):
        return reverse('accounts:profile', kwargs={'pk': self.request.user.pk})


class CoefficientAddView(View):
    def post(self, request, *args, **kwargs):
        percent = request.POST.get('percent')
        coefficient = get_object_or_404(Coefficient, pk=1)
        coefficient.percent = percent
        coefficient.save()
        users = User.objects.all()
        return render(request, "user_detail.html", {"users": users, "user_obj": self.request.user,
                                                    "coefficient": coefficient})
