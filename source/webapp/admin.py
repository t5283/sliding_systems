from django.contrib import admin

from webapp.models import Customer, Application, Order, Gallary, Tire, Section, SlidingSystem, \
    Details, MechanismKit, MechanismKitDetail, Glass, DoorKnob, Option, Color, Coefficient

admin.site.register(Customer)
admin.site.register(Application)
admin.site.register(Order)
admin.site.register(Gallary)
admin.site.register(Tire)
admin.site.register(Section)
admin.site.register(SlidingSystem)
admin.site.register(Details)
admin.site.register(MechanismKit)
admin.site.register(MechanismKitDetail)
admin.site.register(Glass)
admin.site.register(DoorKnob)
admin.site.register(Option)
admin.site.register(Color)
admin.site.register(Coefficient)

