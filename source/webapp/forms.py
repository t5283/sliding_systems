from django import forms
from .models import Application, Order, MechanismKit, SlidingSystem, Coefficient


class SearchForm(forms.Form):
    q = forms.CharField(max_length=100, required=False, label="Поиск")


class ApplicationForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = ["color", "glass", "door_knob", "option", "width",
                  "height", "direction", "address", "loaders_price", "lift"]


class ApplicationForm2(forms.ModelForm):
    class Meta:
        model = Application
        fields = ["color", "glass", "option", "width", "height", "direction", "address", "loaders_price", "lift"]


class MechanismKitForm(forms.ModelForm):
    class Meta:
        model = MechanismKit
        fields = ["name"]


class OrderStatusForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = ["customer", "author", "summa"]


class SlidingSystemForm(forms.ModelForm):
    class Meta:
        model = SlidingSystem
        fields = ["image", "name", "frame_quantity", "coupler_quantity", "drive", "mechanism_kit", "tire", "type", "door_knob"]
