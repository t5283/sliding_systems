from django.test import TestCase, SimpleTestCase
from django.urls import reverse, resolve
from accounts.views import *
from webapp.views import IndexView, GallaryCreateView, GallaryDeleteView, ProductView, SectionAddView, \
    SectionDeleteView, TireAddView, TireDeleteView, GlassAddView, GlassDeleteView, DoorKnobAddView, DoorKnobDeleteView, \
    ColorAddView, ColorDeleteView, OptionAddView, OptionDeleteView, PersonCreateView, OrganizationCreateView, \
    CustomerListView, CustomerDetailView, CustomerDeleteView, PersonEditView, OrganizationEditView, OrderView, \
    OrderAddView, OrderDeleteView, OrderDetailView, ApplicationFormView, ApplicationSlidingSystemChoiceView, \
    OrderTechnicalTaskView, OrderAccessoriesView, OrderStatusChangeView, ApplicationDetailView, ApplicationDeleteView, \
    ApplicationEditView, DetailsCreateView, DetailsDeleteView, MechanismKitFormView, MechanismKitFormDetailDeleteView, \
    MechanismKitFormDetailAddView, MechanismKitDeleteView, ConstructorView, SlidingSystemFormView, \
    SlidingSystemDeleteView, SlidingSystemFormSectionDeleteView, OrderContractView, OrderCommercialProposalView
from django.test import TestCase, SimpleTestCase, Client
from django.urls import reverse, resolve
from accounts.views import *
from webapp.models import Customer, Details, Section, Tire, MechanismKitDetail, Glass, Color, DoorKnob, Option

class TestCustomerViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.list_url = reverse('webapp:customer_list')
        customer = Customer.objects.create(
            name='customer1',
        )
        self.customer_detail_url = reverse('webapp:customer_detail', kwargs={'pk': customer.pk})
        self.person_edit_url = reverse('webapp:person_edit', kwargs={'pk': customer.pk})
        self.org_edit_url = reverse('webapp:organization_edit', kwargs={'pk': customer.pk})
        self.person_add_url = reverse('webapp:person_add')
        self.org_add_url = reverse('webapp:organization_add')


    def test_CustomerListView_GET(self):
        response = self.client.get(self.list_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'customer/customer_list.html')

    def test_CustomerDetailView_GET(self):
        response = self.client.get(self.customer_detail_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'customer/customer_detail.html')

    def test_PersonCreateView(self):
        response = self.client.post(self.person_add_url, {
            'name': 'new_customer',
            'phone': 87070000000,
            'address': 'address2',
            'iin': 000000000000,
            'identification': 000000000000
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/customers/')

    def test_OrganizationCreateView(self):
        response = self.client.post(self.org_add_url, {
            'name': 'new_organization',
            'phone': 87070000000,
            'address': 'address2',
            'bin': 000000000000,
            'iban': 000000000000
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/customers/')

    def test_PersonEditView(self):
        person = Customer.objects.create(name = 'person_name')

        response = self.client.post(self.person_edit_url, {
            'name': 'person_name'
        })
        self.assertEquals(response.status_code, 302)
        person.refresh_from_db()
        self.assertEquals(person.name, 'person_name')

    def test_OrganizationEditView(self):
        org = Customer.objects.create(name='org_name')

        response = self.client.post(self.org_edit_url, {
            'name': 'org_name'
        })
        self.assertEquals(response.status_code, 302)
        org.refresh_from_db()
        self.assertEquals(org.name, 'org_name')

    def test_CustomerDeleteView(self):
        response = self.client.post(reverse('webapp:customer_del', args='1'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/customers/')

class TestProductViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.product_list_url = reverse('webapp:product')
        self.tire_add_url = reverse('webapp:tire_add')
        self.glass_add_url = reverse('webapp:glass_add')
        self.color_add_url = reverse('webapp:color_add')
        self.door_knob_add_url = reverse('webapp:door_knob_add')
        self.option_add_url = reverse('webapp:option_add')
        self.section_add_url = reverse('webapp:section_add')

        color = Color.objects.create(name='color', price=1)
        door = DoorKnob.objects.create(name='door', price=1)
        glass = Glass.objects.create(name='glass', price=1)
        option = Option.objects.create(name='option', price=1)
        tire = Tire.objects.create(name='tire1', price=1)
        self.section_del_url = reverse('webapp:section_del', kwargs={'pk': 1})
        self.tire_del_url = reverse('webapp:tire_del', kwargs={'pk': tire.pk})
        self.glass_del_url = reverse('webapp:glass_del', kwargs={'pk': glass.pk})
        self.door_knob_del_url = reverse('webapp:door_knob_del', kwargs={'pk': door.pk})
        self.color_del_url = reverse('webapp:color_del', kwargs={'pk': color.pk})
        self.option_del_url = reverse('webapp:option_del', kwargs={'pk': option.pk})

    def test_ProductView_GET(self):
        response = self.client.get(self.product_list_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'product/product_list.html')

    def test_TireAddView(self):
        response = self.client.post(self.tire_add_url, {
            'name': 'Tire1',
            'code': 1,
            'price': 1000,
            'quantity': 1,
            'width': 1
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/products/')

    def test_GlassAddView(self):
        response = self.client.post(self.glass_add_url, {
            'image': 'image',
            'name': 'Glass1',
            'code': 1,
            'price': 1000,
            'quantity': 1,
            'width': 1,
            'height': 1
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/products/')

    def test_ColorAddView(self):
        response = self.client.post(self.color_add_url, {
            'image': 'image',
            'name': 'Color1',
            'code': 1,
            'price': 1000
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/products/')

    def test_DoorKnobAddView(self):
        response = self.client.post(self.door_knob_add_url, {
            'image': 'image',
            'name': 'Door1',
            'code': 1,
            'price': 1000
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/products/')

    def test_OptionAddView(self):
        response = self.client.post(self.option_add_url, {
            'image': 'image',
            'name': 'Option1',
            'code': 1,
            'price': 1000
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/products/')

    # def test_SectionAddView(self):
    #     response = self.client.post(self.section_add_url, {
    #         'name': 'Section1',
    #         'code': 1,
    #         'price': 1000,
    #         'type': 'type1',
    #         'quantity': 1
    #     })
    #     self.assertEqual(response.status_code, 302)
    #     self.assertRedirects(response, '/products/')

    def test_SectionDeleteView(self):
        section = Section.objects.create(name='section1', price=1)
        response = self.client.post(self.section_del_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/products/')

    def test_TireDeleteView(self):
        response = self.client.post(self.tire_del_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/products/')

    def test_GlassDeleteView(self):
        response = self.client.post(self.glass_del_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/products/')

    def test_DoorKnobDeleteView(self):
        response = self.client.post(self.door_knob_del_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/products/')

    def test_ColorDeleteView(self):
        response = self.client.post(self.color_del_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/products/')

    def test_OptionDeleteView(self):
        response = self.client.post(self.option_del_url)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/products/')

class UrlTest(SimpleTestCase):

    def test_list_url(self):
        url = reverse('webapp:index')
        self.assertEqual(resolve(url).func.view_class, IndexView)

    def test_list_url(self):
        url = reverse('webapp:gallary_create')
        self.assertEqual(resolve(url).func.view_class, GallaryCreateView)

    def test_list_url(self):
        url = reverse('webapp:gallary_delete', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, GallaryDeleteView)

    def test_list_url(self):
        url = reverse('webapp:product')
        self.assertEqual(resolve(url).func.view_class, ProductView)

    def test_list_url(self):
        url = reverse('webapp:section_add')
        self.assertEqual(resolve(url).func.view_class, SectionAddView)

    def test_list_url(self):
        url = reverse('webapp:section_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, SectionDeleteView)

    def test_list_url(self):
        url = reverse('webapp:tire_add')
        self.assertEqual(resolve(url).func.view_class, TireAddView)

    def test_list_url(self):
        url = reverse('webapp:tire_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, TireDeleteView)

    def test_list_url(self):
        url = reverse('webapp:glass_add')
        self.assertEqual(resolve(url).func.view_class, GlassAddView)

    def test_list_url(self):
        url = reverse('webapp:glass_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, GlassDeleteView)

    def test_list_url(self):
        url = reverse('webapp:door_knob_add')
        self.assertEqual(resolve(url).func.view_class, DoorKnobAddView)

    def test_list_url(self):
        url = reverse('webapp:door_knob_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, DoorKnobDeleteView)

    def test_list_url(self):
        url = reverse('webapp:color_add')
        self.assertEqual(resolve(url).func.view_class, ColorAddView)

    def test_list_url(self):
        url = reverse('webapp:color_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, ColorDeleteView)

    def test_list_url(self):
        url = reverse('webapp:option_add')
        self.assertEqual(resolve(url).func.view_class, OptionAddView)

    def test_list_url(self):
        url = reverse('webapp:option_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, OptionDeleteView)

    def test_list_url(self):
        url = reverse('webapp:person_add')
        self.assertEqual(resolve(url).func.view_class, PersonCreateView)

    def test_list_url(self):
        url = reverse('webapp:organization_add')
        self.assertEqual(resolve(url).func.view_class, OrganizationCreateView)

    def test_list_url(self):
        url = reverse('webapp:customer_list')
        self.assertEqual(resolve(url).func.view_class, CustomerListView)

    def test_list_url(self):
        url = reverse('webapp:customer_detail', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, CustomerDetailView)

    def test_list_url(self):
        url = reverse('webapp:customer_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, CustomerDeleteView)

    def test_list_url(self):
        url = reverse('webapp:person_edit', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, PersonEditView)

    def test_list_url(self):
        url = reverse('webapp:organization_edit', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, OrganizationEditView)

    def test_list_url(self):
        url = reverse('webapp:order')
        self.assertEqual(resolve(url).func.view_class, OrderView)

    def test_list_url(self):
        url = reverse('webapp:order_add')
        self.assertEqual(resolve(url).func.view_class, OrderAddView)

    def test_list_url(self):
        url = reverse('webapp:order_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, OrderDeleteView)

    def test_list_url(self):
        url = reverse('webapp:order_detail', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, OrderDetailView)

    def test_list_url(self):
        url = reverse('webapp:application_form', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, ApplicationFormView)

    def test_list_url(self):
        url = reverse('webapp:application_sliding_system_choice', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, ApplicationSlidingSystemChoiceView)

    def test_list_url(self):
        url = reverse('webapp:technical_task', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, OrderTechnicalTaskView)

    def test_list_url(self):
        url = reverse('webapp:accessories', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, OrderAccessoriesView)

    def test_list_url(self):
        url = reverse('webapp:order_status_change', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, OrderStatusChangeView)

    def test_list_url(self):
        url = reverse('webapp:application_detail', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, ApplicationDetailView)

    def test_list_url(self):
        url = reverse('webapp:application_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, ApplicationDeleteView)

    def test_list_url(self):
        url = reverse('webapp:application_edit', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, ApplicationEditView)

    def test_list_url(self):
        url = reverse('webapp:mechanism_add')
        self.assertEqual(resolve(url).func.view_class, DetailsCreateView)

    def test_list_url(self):
        url = reverse('webapp:mechanism_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, DetailsDeleteView)

    def test_list_url(self):
        url = reverse('webapp:mechanismkit_form')
        self.assertEqual(resolve(url).func.view_class, MechanismKitFormView)

    def test_list_url(self):
        url = reverse('webapp:mechanismkit_detail_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, MechanismKitFormDetailDeleteView)

    def test_list_url(self):
        url = reverse('webapp:mechanismkit_detail_add', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, MechanismKitFormDetailAddView)

    def test_list_url(self):
        url = reverse('webapp:mechanismkit_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, MechanismKitDeleteView)

    def test_list_url(self):
        url = reverse('webapp:constructor')
        self.assertEqual(resolve(url).func.view_class, ConstructorView)

    def test_list_url(self):
        url = reverse('webapp:slidingsystem_add')
        self.assertEqual(resolve(url).func.view_class, SlidingSystemFormView)

    def test_list_url(self):
        url = reverse('webapp:slidingsystem_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, SlidingSystemDeleteView)

    def test_list_url(self):
        url = reverse('webapp:slidingsystem_section_del', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, SlidingSystemFormSectionDeleteView)

    def test_list_url(self):
        url = reverse('webapp:contract', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, OrderContractView)

    def test_list_url(self):
        url = reverse('webapp:commercial_proposal', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, OrderCommercialProposalView)

    def test_list_url(self):
        url = reverse('accounts:register')
        self.assertEqual(resolve(url).func.view_class, RegisterView)

    def test_list_url(self):
        url = reverse('accounts:profile', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, UserDetailView)

    def test_list_url(self):
        url = reverse('accounts:profile', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, UserDetailView)

    def test_list_url(self):
        url = reverse('accounts:change', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, UserChangeView)

    def test_list_url(self):
        url = reverse('accounts:change_password', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, UserPasswordChangeView)