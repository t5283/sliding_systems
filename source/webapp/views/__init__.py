from .customer_view import *
from .product_view import *
from .application_view import *
from .gallary_view import *
from .order_view import *
from .mechanism_view import *
from .constructor_view import *