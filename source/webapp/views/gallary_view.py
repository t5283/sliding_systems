from django.urls import reverse
from django.views.generic import ListView, CreateView, DeleteView
from webapp.models import Gallary


class IndexView(ListView):
    model = Gallary
    template_name = "index.html"
    paginate_by = 8


class GallaryCreateView(CreateView):
    model = Gallary
    fields = ["foto", "description"]
    template_name = "gallary/create.html"

    def get_success_url(self):
        return reverse("webapp:index")


class GallaryDeleteView(DeleteView):
    model = Gallary
    template_name = "index.html"

    def get_success_url(self):
        return reverse("webapp:index")