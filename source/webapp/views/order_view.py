import math

from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.utils.datetime_safe import datetime
from django.views.generic import ListView, CreateView, DeleteView, View

from webapp.forms import ApplicationForm, OrderStatusForm
from webapp.models import Order, Application, SlidingSystem, Coefficient


class OrderView(ListView):#вывод всех продуктов
    template_name = 'order/order_list.html'
    model = Order
    ordering = ["-created_at"]
    paginate_by = 5


class OrderAddView(CreateView):#добавление продукта
    model = Order
    fields = ["customer"]
    template_name = 'order/order_add.html'
    permission_required = "webapp.add_order"

    def get_success_url(self):
        return reverse('webapp:order')


class OrderDeleteView(DeleteView):
    model = Order
    template_name = 'order/order_list.html'
    success_url = reverse_lazy('webapp:order')
    permission_required = "webapp.delete_order"


class OrderDetailView(View):
    def get(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        return render(request, "order/order_detail.html", {"order": order})

    def post(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        sliding_system = get_object_or_404(SlidingSystem, pk=request.POST.get("sliding_system_id"))
        form = ApplicationForm(request.POST, request.FILES)
        if form.is_valid():
            application = Application.objects.create(
                order=order,
                summa=0,
                sliding_system=sliding_system,
                glass=form.cleaned_data["glass"],
                color=form.cleaned_data["color"],
                door_knob=form.cleaned_data["door_knob"],
                option=form.cleaned_data["option"],
                width=form.cleaned_data["width"],
                height=form.cleaned_data["height"],
                direction=form.cleaned_data["direction"],
                loaders_price=form.cleaned_data["loaders_price"],
                lift=form.cleaned_data["lift"],
                address=form.cleaned_data["address"])

            #Расчет размеров створки, стекла, профилей и их количества
            width_sash = (application.width/sliding_system.frame_quantity) - (sliding_system.coupler_quantity*7.5)
            if sliding_system.type == "в нишу":
                width_tire = application.width
                application.sliding_system.tire.width = width_tire
                application.sliding_system.tire.save()
                if sliding_system.drive == "механический":
                    height_sash = application.height - 92
                else:
                    height_sash = application.height - 127
            else:
                width_tire = application.width * 2
                application.sliding_system.tire.width = width_tire
                application.sliding_system.tire.save()
                height_sash = application.height - 23
            application.width_sash = width_sash
            application.height_sash = height_sash
            if sliding_system.door_knob and sliding_system.coupler_quantity > 0 \
                    and sliding_system.name != "Телескопик 2" and sliding_system.name != "Телескопик 3":
                application.door_knob.quantity = sliding_system.coupler_quantity
                application.door_knob.save()
            qty_balk_horizontal = sliding_system.frame_quantity * 2
            qty_balk_coupler = sliding_system.coupler_quantity * 2
            qty_balk_uncoupler = (sliding_system.frame_quantity * 2) - qty_balk_coupler
            qty_tire = whip_quantity(width_tire, 1)
            application.sliding_system.tire.quantity = qty_tire
            qty_whip_horizontal = whip_quantity(width_sash, qty_balk_horizontal)
            qty_whip_coupler = whip_quantity(height_sash, qty_balk_coupler)
            qty_whip_uncoupler = whip_quantity(height_sash, qty_balk_uncoupler)
            application.glass.width = width_sash - 22
            application.glass.height = height_sash - 22
            application.glass.save()
            width_glass_sash = (width_sash - 22)/1000
            height_glass_sash = (height_sash - 22)/1000
            square_glass_sash = width_glass_sash * height_glass_sash
            square_glass_all = square_glass_sash * sliding_system.frame_quantity
            application.glass.quantity = square_glass_all
            application.save()

            #Расчет суммы заказа
            application.summa = sliding_system.mechanism_kit.total + (sliding_system.tire.price * qty_tire)
            section_1 = 0
            section_2 = 0
            section_3 = 0
            for section in application.sliding_system.sections.all():
                if section.type == "без притвора":
                    section.quantity = qty_whip_uncoupler
                    section.quantity_sash = qty_balk_uncoupler
                    section_1 = section.price * qty_whip_uncoupler
                    section.save()
                if section.type == "с притвором":
                    section.quantity = qty_whip_coupler
                    section.quantity_sash = qty_balk_coupler
                    section_2 = section.price * qty_whip_coupler
                    section.save()
                if section.type == "горизонтальный":
                    section.quantity = qty_whip_horizontal
                    section.quantity_sash = qty_balk_horizontal
                    section_3 = section.price * qty_whip_horizontal
                    section.save()
            if application.door_knob:
                application.summa = application.summa + (application.door_knob.price * application.door_knob.quantity)
            if application.option:
                application.summa = application.summa + application.option.price
            application.summa = int(application.summa + section_1 + section_2 + section_3 + \
                                (application.glass.price * math.ceil(square_glass_all)) + application.color.price \
                                + application.loaders_price)
            application.save()
            #Сумма всех заказов
            i = 0
            for application in order.applications.all():
                i = int(application.summa) + i
            order.summa = int(((i/100) * 3) + i)
            coefficient = get_object_or_404(Coefficient, pk=1)
            order.summa = order.summa + ((order.summa/100) * coefficient.percent)
            order.save()
            return render(request, "order/order_detail.html", {"order": order})
        else:
            return render(request, "application/application_form.html",
                          {"order": order, "form": form, "sliding_system": sliding_system})



def whip_quantity(long, balk_qty):
    if long * balk_qty <= 6000:
        return 1
    else:
        return math.ceil((long * balk_qty)/6000)


class OrderTechnicalTaskView(View):
    def get(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        for application in order.applications.all():
            sliding_system = application.sliding_system
            width_sash = (application.width / sliding_system.frame_quantity) - (sliding_system.coupler_quantity * 7.5)
            if sliding_system.type == "в нишу":
                width_tire = application.width
                if sliding_system.drive == "механический":
                    height_sash = application.height - 92
                else:
                    height_sash = application.height - 127
            else:
                width_tire = application.width * 2
                height_sash = application.height - 23
            qty_tire = whip_quantity(width_tire, 1)
            application.sliding_system.tire.quantity = qty_tire
            application.sliding_system.tire.save()
            width_glass_sash = (width_sash - 22) / 1000
            height_glass_sash = (height_sash - 22) / 1000
            square_glass_sash = width_glass_sash * height_glass_sash
            square_glass_all = square_glass_sash * sliding_system.frame_quantity
            application.glass.quantity = math.ceil(square_glass_all)
            application.glass.save()
        return render(request, "order/technical_task.html", {"order": order})


class OrderAccessoriesView(View):
    def get(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        for application in order.applications.all():
            sliding_system = application.sliding_system
            width_sash = (application.width / sliding_system.frame_quantity) - (sliding_system.coupler_quantity * 7.5)
            if sliding_system.type == "в нишу":
                width_tire = application.width
                if sliding_system.drive == "механический":
                    height_sash = application.height - 92
                else:
                    height_sash = application.height - 127
            else:
                width_tire = application.width * 2
                height_sash = application.height - 23
            qty_tire = whip_quantity(width_tire, 1)
            application.sliding_system.tire.quantity = qty_tire
            application.sliding_system.tire.save()
            width_glass_sash = (width_sash - 22) / 1000
            height_glass_sash = (height_sash - 22) / 1000
            square_glass_sash = width_glass_sash * height_glass_sash
            square_glass_all = square_glass_sash * sliding_system.frame_quantity
            application.glass.quantity = math.ceil(square_glass_all)
            application.glass.save()

        return render(request, "order/accessories.html", {"order": order})


class OrderStatusChangeView(View):
    def get(self, request, *args, **kwargs):
        form = OrderStatusForm()
        return render(request, "order/order_detail.html", {"form": form})

    def post(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        order.status = request.POST.get('status')
        order.save()
        return render(request, "order/order_detail.html", {"order": order})


class OrderContractView(View):
    def get(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        date = datetime.now().strftime("%d-%m-%Y")
        summa_90 = (order.summa * 9)/10
        summa_10 = order.summa / 10
        procent_3 = int((order.summa/100) * 3)
        summa_3 = int(order.summa - procent_3)
        return render(request, "order/order_contract.html",
                      {"order": order, "date": date, "summa_90": summa_90, "summa_10": summa_10,
                       "summa_3": summa_3, "procent_3": procent_3})


class OrderCommercialProposalView(View):
    def get(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        date = datetime.now().strftime("%d-%m-%Y")
        summa_90 = (order.summa * 9)/10
        summa_10 = order.summa / 10
        procent_3 = int((order.summa / 100) * 3)
        summa_3 = int(order.summa - procent_3)
        return render(request, "order/commercial_proposal.html",
                      {"order": order, "date": date, "90": summa_90, "10": summa_10,
                       "summa_3": summa_3, "procent_3": procent_3})
