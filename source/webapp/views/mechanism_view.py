from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import View
from django.views.generic import ListView, CreateView, DeleteView

from webapp.forms import MechanismKitForm
from webapp.models import MechanismKit, Details


class DetailsView(ListView):
    model = Details
    template_name = 'product/product_list.html'


class DetailsCreateView(CreateView):
    model = Details
    fields = ["image", "name", "unit_of_measure", "code",  "price"]
    template_name = 'mechanism/mechanism_create.html'

    def get_success_url(self):
        return reverse('webapp:product')


class DetailsDeleteView(DeleteView):
    model = Details
    template_name = 'product/product_list.html'

    def get_success_url(self):
        return reverse('webapp:product')


class MechanismKitView(ListView):
    model = MechanismKit
    template_name = 'constructor/constructor_list.html'


class MechanismBuffer:
    details_list = []
    total = 0


class MechanismKitFormView(View):
    permission_required = "webapp.add_mechanismkit"

    def get(self, request, *args, **kwargs):
        form = MechanismKitForm
        details = Details.objects.all()
        for detail in details:
            detail.quantity = 1
            detail.save()
        return render(request, "mechanism/mechanismkit_create.html",
                      {"form": form, "details": details, "total":MechanismBuffer.total,
                       "details_list": MechanismBuffer.details_list})

    def post(self, request, *args, **kwargs):
        form = MechanismKitForm
        detail = get_object_or_404(Details, pk=int(request.POST.get("detail_id")))
        if detail not in MechanismBuffer.details_list:
            MechanismBuffer.details_list.append(detail)
            MechanismBuffer.total = MechanismBuffer.total + detail.price
        else:
            for buffer_detail in MechanismBuffer.details_list:
                if buffer_detail == detail:
                    buffer_detail.quantity = buffer_detail.quantity + 1
                    buffer_detail.save()
                    MechanismBuffer.total = MechanismBuffer.total + detail.price
        details = Details.objects.all()
        return render(request, "mechanism/mechanismkit_create.html",
                      {"form": form, "details": details, "total": MechanismBuffer.total,
                       "details_list": MechanismBuffer.details_list})


class MechanismKitDeleteView(DeleteView):
    model = MechanismKit
    template_name = 'constructor/constructor_list.html'
    permission_required = "webapp.delete_mechanismkit"

    def get_success_url(self):
        return reverse('webapp:constructor')


class MechanismKitFormDetailDeleteView(View):
    def post(self, request, *args, **kwargs):
        detail = get_object_or_404(Details, pk=request.POST.get("detail_id"))
        if detail in MechanismBuffer.details_list:
            for buffer_detail in MechanismBuffer.details_list:
                if detail == buffer_detail:
                    if buffer_detail.quantity > 1:
                        buffer_detail.quantity = buffer_detail.quantity - 1
                        buffer_detail.save()
                        MechanismBuffer.total = MechanismBuffer.total - detail.price
                    else:
                        MechanismBuffer.details_list.remove(buffer_detail)
                        MechanismBuffer.total = MechanismBuffer.total - detail.price
        return HttpResponse("minus")


class MechanismKitFormDetailAddView(View):
    def post(self, request, *args, **kwargs):
        detail = get_object_or_404(Details, pk=request.POST.get("detail_id"))
        if detail in MechanismBuffer.details_list:
            for buffer_detail in MechanismBuffer.details_list:
                if detail == buffer_detail:
                    buffer_detail.quantity = buffer_detail.quantity + 1
                    buffer_detail.save()
                    MechanismBuffer.total = MechanismBuffer.total + detail.price
        return HttpResponse("plus")
