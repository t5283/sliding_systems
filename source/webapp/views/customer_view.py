from django.db.models import Q
from django.urls import reverse, reverse_lazy
from django.utils.http import urlencode
from django.views.generic import CreateView, ListView, DeleteView, UpdateView, DetailView
from webapp.forms import SearchForm
from webapp.models import Customer


class CustomerListView(ListView):
    model = Customer
    template_name = "customer/customer_list.html"
    paginate_by = 10

    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context["search_form"] = self.form
        if self.search_value:
            context["query"] = urlencode({"q": self.search_value})
        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.search_value:
            query = Q(name__icontains=self.search_value) | Q(organization_name__icontains=self.search_value)
            queryset = queryset.filter(query)
        return queryset

    def get_search_form(self):
        return SearchForm(self.request.GET)

    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data["q"]

class PersonCreateView(CreateView):
    model = Customer
    fields = ["name", "phone", "address", "iin", "identification"]
    template_name = "customer/person_add.html"
    permission_required = "webapp.add_customer"

    def get_success_url(self):
        return reverse("webapp:customer_list")


class OrganizationCreateView(CreateView):
    model = Customer
    fields = ["name", "organization_name", "phone", "address", "bin", "iban"]
    template_name = "customer/organization_add.html"

    def get_success_url(self):
        return reverse("webapp:customer_list")


class CustomerDeleteView(DeleteView):
    model = Customer
    template_name = 'customer/customer_list.html'
    success_url = reverse_lazy('webapp:customer_list')
    permission_required = "webapp.delete_customer"


class PersonEditView(UpdateView):
    model = Customer
    fields = ["name", "phone", "address", "iin", "identification"]
    template_name = 'customer/person_edit.html'
    permission_required = "webapp.change_customer"

    def get_success_url(self):
        return reverse("webapp:customer_list")


class OrganizationEditView(UpdateView):
    model = Customer
    fields = ["organization_name", "name", "phone", "address", "bin", "iban"]
    template_name = 'customer/organization_edit.html'

    def get_success_url(self):
        return reverse("webapp:customer_list")


class CustomerDetailView(DetailView):
    model = Customer
    template_name = 'customer/customer_detail.html'