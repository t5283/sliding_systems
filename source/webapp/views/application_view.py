from django.contrib.auth.mixins import PermissionRequiredMixin
from django.urls import reverse
from django.views.generic import View, DetailView, UpdateView
from django.shortcuts import get_object_or_404, render
from webapp.models import Application, Order, SlidingSystem
from webapp.forms import ApplicationForm, ApplicationForm2


class ApplicationSlidingSystemChoiceView(View):
    def get(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        sliding_systems = SlidingSystem.objects.all()
        return render(request, "application/application_sliding_systems_choice.html",
                      {"order": order, "sliding_systems": sliding_systems})

    def post(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        sliding_systems = SlidingSystem.objects.all()
        return render(request, "application/application_sliding_systems_choice.html",
                      {"order": order, "sliding_systems": sliding_systems})


class ApplicationFormView(View):
    def get(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        sliding_system = get_object_or_404(SlidingSystem, pk=request.GET.get("sliding_system_id"))
        if sliding_system.door_knob == "есть":
            form = ApplicationForm
        else:
            form = ApplicationForm2
        return render(request, "application/application_form.html",
                      {"order": order, "form": form, "sliding_system": sliding_system})

    def post(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        sliding_system = get_object_or_404(SlidingSystem, pk=request.GET.get("sliding_system_id"))
        if sliding_system.door_knob == "есть":
            form = ApplicationForm
        else:
            form = ApplicationForm2
        return render(request, "application/application_form.html",
                      {"order": order, "form": form, "sliding_system": sliding_system})


class ApplicationDetailView(DetailView):
    model = Application
    template_name = "application/application_detail.html"


class ApplicationDeleteView(PermissionRequiredMixin, View):
    permission_required = "webapp.delete_application"

    def post(self, request, *args, **kwargs):
        application = get_object_or_404(Application, pk=kwargs["pk"])
        order = application.order
        application.summa = application.summa + ((application.summa / 100) * 3)
        order.summa = int(order.summa) - int(application.summa)
        order.save()
        application.delete()
        return render(request, "order/order_detail.html", {"order": order})


class ApplicationEditView(PermissionRequiredMixin, UpdateView):
    model = Application
    fields = ["sliding_system", "color", "glass", "door_knob", "option", "width",
              "height", "direction", "address", "loaders_price", "lift"]
    template_name = 'application/application_edit.html'
    permission_required = "webapp.change_application"

    def get_success_url(self):
        return reverse("webapp:application_detail", kwargs={"pk": self.get_object().pk})
