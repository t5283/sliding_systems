from django.urls import reverse
from django.views.generic import ListView, CreateView, DeleteView
from webapp.models import Frame


class FrameView(ListView):
    model = Frame
    template_name = 'constructor/constructor_list.html'


class FrameCreateView(CreateView):
    model = Frame
    fields = ["type", "profile_sash", "profile_sash_width", "quantity", "price_profile_sash_vertical",  "price_profile_sash_top_bottom"]
    template_name = 'constructor/frame_create.html'

    def get_success_url(self):
        return reverse('webapp:constructor')


class FrameDeleteView(DeleteView):
    model = Frame
    template_name = 'constructor/constructor_list.html'

    def get_success_url(self):
        return reverse('webapp:constructor')