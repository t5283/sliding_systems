from django.shortcuts import render
from django.views.generic import CreateView, DeleteView, View
from django.urls import reverse, reverse_lazy

from webapp.models import Details, Section, Tire, MechanismKitDetail, Glass, Color, DoorKnob, Option



class ProductView(View):#вывод всех продуктов
    def get(self, request, *args, **kwargs):
        tire = Tire.objects.all()
        section = Section.objects.all()
        mechanism = Details.objects.all()
        glass = Glass.objects.all()
        color = Color.objects.all()
        door_knob = DoorKnob.objects.all()
        option = Option.objects.all()
        return render(request, "product/product_list.html",
                      {"tire_list": tire, "section_list": section, "mechanism_list": mechanism, "glass_list": glass,
                       "color_list": color, "door_knob_list": door_knob, "option_list": option})

    def post(self, request, *args, **kwargs):
        tire = Tire.objects.all()
        section = Section.objects.all()
        mechanism = MechanismKitDetail.objects.all()
        glass = Glass.objects.all()
        color = Color.objects.all()
        door_knob = DoorKnob.objects.all()
        option = Option.objects.all()
        return render(request, "product/product_list.html",
                      {"tire_list": tire, "section_list": section, "mechanism_list": mechanism, "glass_list": glass,
                       "color_list": color, "door_knob_list": door_knob, "option_list": option})


class TireAddView(CreateView):
    template_name = 'product/tire_add.html'
    model = Tire
    fields = ["name", "code", "price"]
    permission_required = "webapp.add_tire"

    def get_success_url(self):
        return reverse('webapp:product')


class GlassAddView(CreateView):
    template_name = 'product/glass_add.html'
    model = Glass
    fields = ["image", "name", "code", "price"]
    permission_required = "webapp.add_glass"

    def get_success_url(self):
        return reverse('webapp:product')

class ColorAddView(CreateView):
    template_name = 'product/color_add.html'
    model = Color
    fields = ["image", "name", "code", "price"]
    permission_required = "webapp.add_color"

    def get_success_url(self):
        return reverse('webapp:product')

class DoorKnobAddView(CreateView):
    template_name = 'product/door_knob_add.html'
    model = DoorKnob
    fields = ["image", "name", "code", "price"]
    permission_required = "webapp.add_doorknob"

    def get_success_url(self):
        return reverse('webapp:product')


class OptionAddView(CreateView):
    template_name = 'product/option_add.html'
    model = Option
    fields = ["image", "name", "code", "price"]
    permission_required = "webapp.add_option"

    def get_success_url(self):
        return reverse('webapp:product')


class SectionAddView(CreateView):
    template_name = 'product/section_add.html'
    model = Section
    fields = ["name", "code", "price", "type"]
    permission_required = "webapp.add_section"

    def get_success_url(self):
        return reverse('webapp:product')


class SectionDeleteView(DeleteView):
    template_name = 'product/product_list.html'
    model = Section
    success_url = reverse_lazy('webapp:product')
    permission_required = "webapp.delete_section"


class TireDeleteView(DeleteView):
    template_name = 'product/product_list.html'
    model = Tire
    success_url = reverse_lazy('webapp:product')
    permission_required = "webapp.delete_tire"


class GlassDeleteView(DeleteView):
    template_name = 'product/product_list.html'
    model = Glass
    success_url = reverse_lazy('webapp:product')
    permission_required = "webapp.delete_glass"


class DoorKnobDeleteView(DeleteView):
    template_name = 'product/product_list.html'
    model = DoorKnob
    success_url = reverse_lazy('webapp:product')
    permission_required = "webapp.delete_doorknob"


class ColorDeleteView(DeleteView):
    template_name = 'product/product_list.html'
    model = Color
    success_url = reverse_lazy('webapp:product')
    permission_required = "webapp.delete_color"


class OptionDeleteView(DeleteView):
    template_name = 'product/product_list.html'
    model = Option
    success_url = reverse_lazy('webapp:product')
    permission_required = "webapp.delete_option"
