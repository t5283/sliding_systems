from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import View
from django.views.generic import CreateView, DeleteView

from webapp.forms import MechanismKitForm, SlidingSystemForm
from webapp.models import SlidingSystem, MechanismKit, MechanismKitDetail, Details, Section, SlidingSystemSection
from webapp.views import MechanismBuffer


class ConstructorView(View):
    def get(self, request, *args, **kwargs):
        sliding_systems = SlidingSystem.objects.all()
        mechanismkit_list = MechanismKit.objects.all()
        MechanismBuffer.total = 0
        MechanismBuffer.details_list = []
        SectionBuffer.section_list = []
        return render(request, "constructor/constructor_list.html",
                      {"mechanismkit_list": mechanismkit_list, "slidingsystem_list": sliding_systems})

    def post(self, request, *args, **kwargs):
        sliding_systems = SlidingSystem.objects.all()
        mechanismkit_list = MechanismKit.objects.all()
        if request.POST.get("mechanismkit") == "mechanismkit":
            form = MechanismKitForm(data=request.POST)
            if form.is_valid():
                mechanismkit = MechanismKit.objects.create(
                    name=form.cleaned_data["name"],
                    total=int(MechanismBuffer.total))
                for detail in MechanismBuffer.details_list:
                    MechanismKitDetail.objects.create(
                        mechanism_kit=mechanismkit,
                        image=detail.image,
                        name=detail.name,
                        unit_of_measure=detail.unit_of_measure,
                        code=detail.code,
                        price=detail.price,
                        quantity=detail.quantity)
            MechanismBuffer.total = 0
            MechanismBuffer.details_list = []
            for m_detail in Details.objects.all():
                m_detail.quantity = 1
                m_detail.save()
        if request.POST.get("sliding_system") == "sliding_system":
            form = SlidingSystemForm(request.POST, request.FILES)
            if form.is_valid():
                sliding_system = SlidingSystem.objects.create(
                    image=form.cleaned_data["image"],
                    name=form.cleaned_data["name"],
                    frame_quantity=form.cleaned_data["frame_quantity"],
                    coupler_quantity=form.cleaned_data["coupler_quantity"],
                    mechanism_kit=form.cleaned_data["mechanism_kit"],
                    tire=form.cleaned_data["tire"],
                    type=form.cleaned_data["type"],
                    drive=form.cleaned_data["drive"],
                    door_knob=form.cleaned_data["door_knob"])
                for section in SectionBuffer.section_list:
                    SlidingSystemSection.objects.create(
                        sliding_system=sliding_system,
                        name=section.name,
                        type=section.type,
                        code=section.code,
                        price=section.price,
                        quantity=section.quantity)
            SectionBuffer.section_list = []
        return render(request, "constructor/constructor_list.html",
                      {"mechanismkit_list": mechanismkit_list, "slidingsystem_list": sliding_systems})


class SectionBuffer:
    section_list = []


class SlidingSystemFormView(View):
    permission_required = "webapp.add_slidingsystem"
    def get(self, request, *args, **kwargs):
        form = SlidingSystemForm
        sections = Section.objects.all()
        return render(request, "constructor/slidingsystem_form.html",
                      {"form": form, "sections": sections, "section_list": SectionBuffer.section_list})

    def post(self, request, *args, **kwargs):
        form = SlidingSystemForm
        section = get_object_or_404(Section, pk=int(request.POST.get("section_id")))
        if section not in SectionBuffer.section_list:
            SectionBuffer.section_list.append(section)
        sections = Section.objects.all()
        return render(request, "constructor/slidingsystem_form.html",
                      {"form": form, "sections": sections, "section_list": SectionBuffer.section_list})


class SlidingSystemDeleteView(DeleteView):
    model = SlidingSystem
    template_name = 'constructor/constructor_list.html'
    permission_required = "webapp.delete_slidingsystem"

    def get_success_url(self):
        return reverse('webapp:constructor')


class SlidingSystemFormSectionDeleteView(View):
    def post(self, request, *args, **kwargs):
        section = get_object_or_404(Section, pk=kwargs["pk"])
        if section in SectionBuffer.section_list:
            SectionBuffer.section_list.remove(section)
        form = MechanismKitForm
        sections = Section.objects.all()
        return render(request, "constructor/slidingsystem_form.html",
                      {"form": form, "sections": sections, "section_list": SectionBuffer.section_list})
