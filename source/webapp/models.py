from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import BaseValidator
from django.utils.deconstruct import deconstructible
from django.utils.translation.trans_null import ngettext_lazy, _


@deconstructible
class DecimalValidator:
    """
    Validate that the input does not exceed the maximum number of digits
    expected, otherwise raise ValidationError.
    """
    messages = {
        'invalid': _('Enter a number.'),
        'max_digits': ngettext_lazy(
            'Количество символов не должно превышать %(max).',
            'Количество символов не должно превышать %(max)',
            'max'
        ),
        'max_decimal_places': ngettext_lazy(
            'Ensure that there are no more than %(max)s decimal place.',
            'Ensure that there are no more than %(max)s decimal places.',
            'max'
        ),
        'max_whole_digits': ngettext_lazy(
            'Ensure that there are no more than %(max)s digit before the decimal point.',
            'Ensure that there are no more than %(max)s digits before the decimal point.',
            'max'
        ),
    }

    def __init__(self, max_digits, decimal_places):
        self.max_digits = max_digits
        self.decimal_places = decimal_places

    def __call__(self, value):
        digit_tuple, exponent = value.as_tuple()[1:]
        if exponent in {'F', 'n', 'N'}:
            raise ValidationError(self.messages['invalid'], code='invalid', params={'value': value})
        if exponent >= 0:
            # A positive exponent adds that many trailing zeros.
            digits = len(digit_tuple) + exponent
            decimals = 0
        else:
            # If the absolute value of the negative exponent is larger than the
            # number of digits, then it's the same as the number of digits,
            # because it'll consume all of the digits in digit_tuple and then
            # add abs(exponent) - len(digit_tuple) leading zeros after the
            # decimal point.
            if abs(exponent) > len(digit_tuple):
                digits = decimals = abs(exponent)
            else:
                digits = len(digit_tuple)
                decimals = abs(exponent)
        whole_digits = digits - decimals

        if self.max_digits is not None and digits > self.max_digits:
            raise ValidationError(
                self.messages['max_digits'],
                code='max_digits',
                params={'max': self.max_digits, 'value': value},
            )
        if self.decimal_places is not None and decimals > self.decimal_places:
            raise ValidationError(
                self.messages['max_decimal_places'],
                code='max_decimal_places',
                params={'max': self.decimal_places, 'value': value},
            )
        if (self.max_digits is not None and self.decimal_places is not None and
                whole_digits > (self.max_digits - self.decimal_places)):
            raise ValidationError(
                self.messages['max_whole_digits'],
                code='max_whole_digits',
                params={'max': (self.max_digits - self.decimal_places), 'value': value},
            )

    def __eq__(self, other):
        return (
            isinstance(other, self.__class__) and
            self.max_digits == other.max_digits and
            self.decimal_places == other.decimal_places
        )


@deconstructible
class MinLengthValidator(BaseValidator):
    message = 'Значение "%(value)" имеет длину %(show_value)! Длина должна быть не менее %(limit_value) символов!'
    code = 'too_short'

    def compare(self, a, b):
        return a < b

    def clean(self, x):
        return len(x)


@deconstructible
class MaxLengthValidator(BaseValidator):
    message = 'Значение "%(value)" имеет длину %(show_value)! Длина должна быть не более %(limit_value) символов!'
    code = 'too_short'

    def compare(self, a, b):
        return a > b

    def clean(self, x):
        return len(x)


class Gallary(models.Model):
    foto = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name="Картинка")
    description = models.TextField(null=True, blank=True, max_length=3000, verbose_name="Описание",
                                   validators=[MaxLengthValidator(3000)])



class Details(models.Model):
    unit_of_measure_choices = [("штук", "штук"), ("комплект", "комплект")]

    image = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name="Картинка")
    name = models.CharField(max_length=150, verbose_name="Наименование детали комплекта механизмов",
                            validators=[MaxLengthValidator(150)])
    unit_of_measure = models.CharField(max_length=50, choices=unit_of_measure_choices, verbose_name="Ед. измерения")
    code = models.CharField(max_length=50, null=True, blank=True, verbose_name="Код",
                            validators=[MaxLengthValidator(50)])
    price = models.DecimalField(max_digits=7, decimal_places=0, verbose_name="Стоимость",
                                validators=[DecimalValidator(7, 0)])
    quantity = models.PositiveIntegerField(default=1, null=True, blank=True, verbose_name="Количество")

    def __str__(self):
        return self.name


class MechanismKitDetail(models.Model):
    unit_of_measure_choices = [("штук", "штук"), ("комплект", "комплект")]

    mechanism_kit = models.ForeignKey("webapp.MechanismKit", on_delete=models.CASCADE,
                                      verbose_name="Комплект механизмов", related_name="details")
    image = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name="Картинка")
    name = models.CharField(max_length=150, verbose_name="Наименование детали комплекта механизмов",
                            validators=[MaxLengthValidator(150)])
    unit_of_measure = models.CharField(max_length=50, choices=unit_of_measure_choices, verbose_name="Ед. измерения")
    code = models.CharField(max_length=50, null=True, blank=True, verbose_name="Код",
                            validators=[MaxLengthValidator(50)])
    price = models.DecimalField(max_digits=7, decimal_places=0, verbose_name="Стоимость",
                                validators=[DecimalValidator(7, 0)])
    quantity = models.PositiveIntegerField(default=1, null=True, blank=True, verbose_name="Количество")

    def __str__(self):
        return self.name


class MechanismKit(models.Model):
    name = models.CharField(max_length=150, verbose_name="Наименование комплекта механизмов",
                            validators=[MaxLengthValidator(150)])
    total = models.DecimalField(max_digits=7, decimal_places=0, verbose_name="Стоимость комплекта")

    def __str__(self):
        return self.name


class Section(models.Model):
    type_choices = [("без притвора", "без притвора"), ("с притвором", "с притвором"),
                    ("горизонтальный", "горизонтальный")]

    name = models.CharField(max_length=100, verbose_name="Наименование профиля", validators=[MaxLengthValidator(150)])
    code = models.CharField(max_length=50, null=True, blank=True, verbose_name="Код",
                            validators=[MaxLengthValidator(50)])
    price = models.DecimalField(max_digits=7, decimal_places=0, verbose_name="Стоимость профиля",
                                validators=[DecimalValidator(7, 0)])
    type = models.CharField(max_length=20, choices=type_choices, verbose_name="Вид профиля")
    quantity = models.PositiveIntegerField(default=1, null=True, blank=True, verbose_name="Количество")

    def __str__(self):
        return f'{self.name} - {self.type}'


class SlidingSystemSection(models.Model):
    type_choices = [("без притвора", "без притвора"), ("с притвором", "с притвором"),
                    ("горизонтальный", "горизонтальный")]

    sliding_system = models.ForeignKey("webapp.SlidingSystem", on_delete=models.CASCADE,
                                      verbose_name="Комплект механизмов", related_name="sections")
    name = models.CharField(max_length=100, verbose_name="Наименование профиля", validators=[MaxLengthValidator(100)])
    code = models.CharField(max_length=50, null=True, blank=True, verbose_name="Код",
                            validators=[MaxLengthValidator(50)])
    price = models.DecimalField(max_digits=7, decimal_places=0, verbose_name="Стоимость профиля",
                                validators=[DecimalValidator(7, 0)])
    type = models.CharField(max_length=20, choices=type_choices, verbose_name="Вид профиля")
    quantity = models.PositiveIntegerField(default=1, null=True, blank=True, verbose_name="Количество")
    quantity_sash = models.PositiveIntegerField(default=1, null=True, blank=True, verbose_name="Количество балок")

    def __str__(self):
        return f'{self.name} - {self.type}'


class Tire(models.Model):
    name = models.CharField(max_length=100, verbose_name="Наименование шины", validators=[MaxLengthValidator(100)])
    code = models.CharField(max_length=50, null=True, blank=True, verbose_name="Код",
                            validators=[MaxLengthValidator(50)])
    price = models.DecimalField(max_digits=7, decimal_places=0, verbose_name="Стоимость шины",
                                validators=[DecimalValidator(7, 0)])
    quantity = models.PositiveIntegerField(default=1, null=True, blank=True, verbose_name="Количество шины")
    width = models.PositiveIntegerField(default=0, null=True, blank=True, verbose_name="Длинна шины")

    def __str__(self):
        return self.name


class SlidingSystem(models.Model):
    type_choices = [("в нишу", "в нишу"), ("на нишу", "на нишу")]
    drive_choices = [("механический", "механический"), ("электрический", "электрический")]
    door_knob_choices = [("есть", "есть"), ("нет", "нет")]

    name = models.CharField(max_length=150, verbose_name="Наименование раздвижной системы",
                            validators=[MaxLengthValidator(150)])
    image = models.ImageField(upload_to='user_pics', verbose_name="Картинка")
    frame_quantity = models.PositiveIntegerField(default=1, verbose_name="Количество створок")
    coupler_quantity = models.PositiveIntegerField(default=0, verbose_name="Количество притворов")
    mechanism_kit = models.ForeignKey("webapp.MechanismKit", on_delete=models.CASCADE,
                                      verbose_name="Комплект механизмов")
    tire = models.ForeignKey("webapp.Tire", on_delete=models.CASCADE, verbose_name="Шина")
    type = models.CharField(max_length=20, choices=type_choices, verbose_name="Тип")
    drive = models.CharField(max_length=20, choices=drive_choices, verbose_name="Привод")
    door_knob = models.CharField(max_length=20, choices=door_knob_choices, verbose_name="Дверная ручка")

    def __str__(self):
        return f'{self.drive}-{self.name}-{self.type}'


class Glass(models.Model):

    image = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name="Картинка")
    name = models.CharField(max_length=100, verbose_name="Наименование", validators=[MaxLengthValidator(100)])
    code = models.CharField(max_length=50, null=True, blank=True, verbose_name="Код",
                            validators=[MaxLengthValidator(50)])
    price = models.DecimalField(max_digits=7, decimal_places=0, verbose_name="Цена",
                                validators=[DecimalValidator(7, 0)])
    quantity = models.DecimalField(max_digits=7, decimal_places=0, default=1, null=True, blank=True, verbose_name="Количество")
    width = models.PositiveIntegerField(verbose_name="Ширина стекла", null=True, blank=True, default=0)
    height = models.PositiveIntegerField(verbose_name="Высота стекла", null=True, blank=True, default=0)

    def __str__(self):
        return self.name


class DoorKnob(models.Model):

    image = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name="Картинка")
    name = models.CharField(max_length=100, verbose_name="Наименование", validators=[MaxLengthValidator(100)])
    code = models.CharField(max_length=50, null=True, blank=True, verbose_name="Код",
                            validators=[MaxLengthValidator(50)])
    price = models.DecimalField(max_digits=7, decimal_places=0, verbose_name="Цена",
                                validators=[DecimalValidator(7, 0)])
    quantity = models.PositiveIntegerField(default=1, null=True, blank=True, verbose_name="Количество")

    def __str__(self):
        return self.name


class Color(models.Model):

    image = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name="Картинка")
    name = models.CharField(max_length=100, verbose_name="Наименование", validators=[MaxLengthValidator(100)])
    code = models.CharField(max_length=50, null=True, blank=True, verbose_name="Код",
                            validators=[MaxLengthValidator(50)])
    price = models.DecimalField(max_digits=7, decimal_places=0, verbose_name="Цена",
                                validators=[DecimalValidator(7, 0)])
    quantity = models.PositiveIntegerField(default=1, null=True, blank=True, verbose_name="Количество")

    def __str__(self):
        return f'{self.name} {self.code}'


class Option(models.Model):

    image = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name="Картинка")
    name = models.CharField(max_length=100, verbose_name="Наименование", validators=[MaxLengthValidator(100)])
    code = models.CharField(max_length=50, null=True, blank=True, verbose_name="Код",
                            validators=[MaxLengthValidator(50)])
    price = models.DecimalField(max_digits=7, decimal_places=0, verbose_name="Цена",
                                validators=[DecimalValidator(7, 0)])
    quantity = models.PositiveIntegerField(default=1, null=True, blank=True, verbose_name="Количество")

    def __str__(self):
        return self.name


class Application(models.Model):
    direction_choices = [("в право", "в право"), ("в лево", "в лево"), ("синхронное", "синхронное")]

    order = models.ForeignKey("webapp.Order", verbose_name="Заявка", on_delete=models.CASCADE,
                                 related_name="applications")
    sliding_system = models.ForeignKey("webapp.SlidingSystem", on_delete=models.CASCADE,
                                        verbose_name="Наименование раздвижной системы")
    color = models.ForeignKey("webapp.Color", on_delete=models.CASCADE, verbose_name="Цвет профиля")
    glass = models.ForeignKey("webapp.Glass", on_delete=models.CASCADE, verbose_name="Стекло")
    door_knob = models.ForeignKey("webapp.DoorKnob", on_delete=models.CASCADE, null=True, blank=True,
                                  verbose_name="Дверная ручка")
    option = models.ForeignKey("webapp.Option", on_delete=models.CASCADE, null=True, blank=True,
                               verbose_name="Дополнительная опция")
    width = models.PositiveIntegerField(verbose_name="Ширина проема в мм")
    height = models.PositiveIntegerField(verbose_name="Высота проема в мм")
    width_sash = models.PositiveIntegerField(verbose_name="Ширина створки в мм", null=True, blank=True,)
    height_sash = models.PositiveIntegerField(verbose_name="Высота створки в мм", null=True, blank=True,)
    direction = models.CharField(max_length=30, choices=direction_choices, default="right", verbose_name="Направление")
    summa = models.DecimalField(max_digits=8, decimal_places=0, verbose_name="Сумма")
    address = models.CharField(max_length=150, default="не указан", null=True, blank=True, verbose_name="Адрес объекта",
                               validators=[MaxLengthValidator(150)])
    loaders_price = models.PositiveIntegerField(default=0, null=True, blank=True, verbose_name="Стоимость грузчиков")
    lift = models.CharField(max_length=100, default="не указан", null=True, blank=True, verbose_name="Наличие лифта",
                            validators=[MaxLengthValidator(100)])

    width_sash = models.PositiveIntegerField(default=0, null=True, blank=True, verbose_name="Ширина створки")
    height_sash = models.PositiveIntegerField(default=0, null=True, blank=True, verbose_name="Высота створки")
    qty_profile_sash_vertical = models.PositiveIntegerField(default=0, null=True, blank=True,
                                                            verbose_name="Количество вертикального профиля створки")
    qty_profile_sash_top_bottom = models.PositiveIntegerField(default=0, null=True, blank=True,
                                                            verbose_name="Количество верх.-нижних профилей створки")
    width_top_tire = models.PositiveIntegerField(default=0, null=True, blank=True, verbose_name="Длинна верхней шины")
    # width_profile_frame = models.PositiveIntegerField(default=0, null=True, blank=True, verbose_name="Длинна профиль рамы")

    def __str__(self):
        return f'{self.sliding_system}'



class Order(models.Model):
    status_choices = [("новая", "новая"), ("в работе", "в работе"), ("завершена", "завершена")]

    customer = models.ForeignKey("webapp.Customer", on_delete=models.CASCADE,
                                 verbose_name="Заказчик", related_name="order")
    author = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, default=1, verbose_name="автор")
    summa = models.DecimalField(max_digits=8, null=True, blank=True, decimal_places=0, default=0, verbose_name="Сумма")
    status = models.CharField(max_length=30, choices=status_choices, default="новая", verbose_name="Status")
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.pk} - Заявка'


class Customer(models.Model):
    name = models.CharField(max_length=50, null=False, verbose_name="ФИО контрагента",
                            validators=[MaxLengthValidator(50)])
    organization_name = models.CharField(max_length=150, null=True, blank=True, verbose_name="Наименование организации",
                                         validators=[MaxLengthValidator(150)])

    address = models.CharField(max_length=150, null=True, blank=True, verbose_name="Адрес объекта",
                               validators=[MaxLengthValidator(150)])
    phone = models.CharField(max_length=12, null=True, blank=True, verbose_name="Контактный телефон",
                             validators=[MinLengthValidator(6), MaxLengthValidator(12)])
    iin = models.DecimalField(max_digits=12, null=True, blank=True, decimal_places=0, verbose_name="ИИН",
                              validators=[DecimalValidator(12, 0)])
    identification = models.CharField(max_length=150, null=True, blank=True, verbose_name="Удостоверение личности",
                                      validators=[MaxLengthValidator(150)])

    bin = models.DecimalField(max_digits=12, null=True, blank=True, decimal_places=0, verbose_name="БИН",
                              validators=[DecimalValidator(12, 0)])
    iban = models.CharField(max_length=150, null=True, blank=True, verbose_name="Расчетный счет",
                            validators=[MaxLengthValidator(150)])

    def __str__(self):
        return self.name


class Coefficient(models.Model):
    percent = models.PositiveIntegerField(default=0, verbose_name="Процент")
